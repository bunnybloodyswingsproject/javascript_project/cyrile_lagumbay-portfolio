//NAVBAR
const menuBtn = document.querySelector(".menu-btn");
const navID = document.querySelector(".nav_list");
const navLinks = document.querySelectorAll(".nav_item");
let menuOpen = false;

menuBtn.addEventListener("click", () =>{ 
	if(!menuOpen) {
		menuBtn.classList.add("open");
		navID.classList.add("show");
		menuOpen = true;
	}else{
		menuBtn.classList.remove("open");
		navID.classList.remove("show");
		menuOpen = false;
	}

	//animate
	navLinks.forEach((link, index) => {
		link.style.animation = `navLinksFade 0.5s ease forwards ${index / 7 + 0.3}s`;
		console.log(index/7);
	})

	//remove navlist if forgot to close and scroll down

	const appearOptions = {
	threshold: 0,
	rootMargin: "0px 0px -250px 0px"
};
	const appearOnScroll = new IntersectionObserver(function(entries, appearOnScroll) {
		entries.forEach(entry => {
			if(!entry.isIntersecting) {
				entry.target.classList.remove("show");
				entry.target.classList.remove("open");
			}else{
				return;
			}
		});
	}, appearOptions);

	appearOnScroll.observe(menuBtn);
	appearOnScroll.observe(navID);
})

  //Hover effect
  //Dammit! the path of this package is so hard to find
  //When using this package, start in the file where the parent is residing
  //then there, start the path finding like in this. the starting path is
  //the index.html


    new hoverEffect({
        parent: document.querySelector('.home_image'),
        intensity: 0.1,
        image1: "./assets/images/cyrile.png",
        image2: "./assets/images/cy.png",
        displacementImage: "./assets/images/heightMap.png"
   });



//Parallax home scroll
let atScroll = false;
let parallaxTitle = document.querySelectorAll(".parallax-title");
let parallaxTitleReverse = document.querySelectorAll(".parallax-title-reverse")

const scrollProgress = () => {
	atScroll = true;
}

const raf = () => {
	if(atScroll) {
		parallaxTitle.forEach((element, index) => {
			element.style.transform = "translateX(" + window.scrollY / 8 + "%)";
		});

		parallaxTitleReverse.forEach((element, index) => {
			element.style.transform = "translateX(-" + window.scrollY / 8 + "%)";
		});

		atScroll = false;
	}

	requestAnimationFrame(raf);
}

requestAnimationFrame(raf);
window.addEventListener("scroll", scrollProgress);


//Programming Stats
const statsBtn = document.querySelector(".stats_logo");
const programStats = document.querySelector(".programming_stats");
const statsLists = document.querySelectorAll(".skill_lists");
const statsClose = document.querySelector(".stats_close");
let statsOpen = false;

statsBtn.addEventListener("click", () =>{ 
	if(!statsOpen) {
		programStats.classList.add("reveal_programmingStats");
		statsOpen = true;
	}else{
		programStats.classList.remove("reveal_programmingStats");
		statsOpen = false;
	}

	//animate
	statsLists.forEach((stat, index) => {
		stat.style.animation = `navLinksFade 0.5s ease forwards ${index / 7 + 0.3}s`;
	})
})

statsClose.addEventListener("click", () => {
	if(statsOpen) {
		programStats.classList.remove("reveal_programmingStats");
		statsOpen = false;
	}
});

//Animate Stats logo

const arrow = document.querySelector(".fa-location-arrow");
const graph = document.querySelector(".fa-chart-bar");

function removeArrow(statsLogo) {
	statsLogo.classList.add("stats_white");
	statsLogo.classList.remove("stats_logo");
	arrow.style.display = "inline-block";
	graph.style.display = "none";
}

function normalLogo(statsLogo) {
	statsLogo.classList.remove("stats_white");
	statsLogo.classList.add("stats_logo");
	arrow.style.display = "none";
	graph.style.display = "inline-block";
}

//Slider Testimonies logo

let counter = 1;
setInterval(function() {
	document.getElementById("radio" + counter).checked = true;
	counter++;
	if(counter > 4) {
		counter = 1;
	}
}, 5000);

//Call To action
const actionBox = document.querySelectorAll(".action_box");
const dropDownList = document.querySelectorAll(".dropDownList");

actionBox.forEach(n => n.addEventListener("click", removeDropDownList));

function removeDropDownList(event) {
	e = event.currentTarget;
	nextSibs = e.firstElementChild.nextElementSibling;
	pointLogo = e.firstElementChild.firstElementChild.nextElementSibling;
	peaceLogo = e.firstElementChild.firstElementChild;
	actionTitle = e.firstElementChild.lastElementChild;
	if(nextSibs.classList.contains("removeElement")) {
		nextSibs.classList.remove("removeElement");
		pointLogo.classList.remove("removeElement");
		peaceLogo.classList.add("removeElement");
		actionTitle.style.color = "white";
	}else{
		nextSibs.classList.add("removeElement");
		pointLogo.classList.add("removeElement");
		peaceLogo.classList.remove("removeElement");
		actionTitle.style.color = "black";
	}
}